<?php

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'AAO_MANAGER_LIGHT' 	=> 'Reduced trial manager',
	'AAO_MANAGER_FULL' 	=> 'Full trial manager',
));

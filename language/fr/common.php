<?php

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'AAO_MANAGER_LIGHT' 	=> 'Gestionnaire de procès réduit',
	'AAO_MANAGER_FULL' 	=> 'Gestionnaire de procès complet',
));

<?php

namespace unas\aaolink\event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class main_listener implements EventSubscriberInterface
{
    /**
     * Assign functions defined in this class to event listeners in the core
     *
     * @return array
     */
    static public function getSubscribedEvents()
    {
        return [
            'core.user_setup' => 'load_language_on_setup',
			'core.page_header' => 'generate_aao_links',
        ];
    }
	
	/* @var \phpbb\template\template */
	protected $template;
	
	/** @var \phpbb\db\driver\driver_interface */
	protected $db;
	
	/* @var \phpbb\user */
	protected $user;

	public function __construct(\phpbb\template\template $template, \phpbb\db\driver\driver_interface $db, \phpbb\user $user)
	{
		$this->template = $template;
		$this->db = $db;
		$this->user = $user;
	}

    /**
     * Load the language file
     *
     * @param \phpbb\event\data $event The event object
     */
    public function load_language_on_setup($event)
    {
        $lang_set_ext = $event['lang_set_ext'];
        $lang_set_ext[] = [
            'ext_name' => 'unas/aaolink',
            'lang_set' => 'common',
        ];
        $event['lang_set_ext'] = $lang_set_ext;
    }
	
	/**
     * Generate all AAO links
     *
     * @param \phpbb\event\data $event The event object
     */
    public function generate_aao_links($event)
    {
        $this->template->assign_vars(array(
			'U_AAO_ROOT'	=> '..',
		));
		
		$query = $this->db->sql_query('SELECT * FROM liste_proces
			WHERE
				(
					auteur='.$this->user->data['user_id'].' OR
					collaborateurs LIKE "%['.$this->user->data['user_id'].']%"
				)
				AND jouable=0 ORDER BY id');
		
		while($trial = $this->db->sql_fetchrow($query))
		{
			$this->template->assign_block_vars('trialrow', array(
				'AAO_TRIAL_ID' => $trial['id'],
				'AAO_TRIAL_TITLE' => $trial['titre']
			));
		}
    }
}
